import sbt._

object Dependencies {

  object Versions {
    val scala = "2.11.7"
    val akka = "2.3.12"
    val logback = "1.1.3"
  }

  lazy val server = Def.setting(Seq())
}
