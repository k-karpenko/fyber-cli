package fyber.cli

import fyber.cli.v1.{StandardCliProcessor, CliProcessor}
import CliProcessor.ProcessorState

object App {
  import common._

  def main(args: Array[String]): Unit = {
    val processor = new StandardCliProcessor

    var isStopped = false
    while(!isStopped) {
      Console.out.print("\n>> ")
      val line = Console.in.readLine()
      processor.process(line.split("\\s")) match {
        case Output.Error(message) => Console.err.println(s"[ERR], $message")
        case Output.Str(message) => Console.out.println(message)
        case Output.Empty =>
        case Output.Exit =>
          Console.out.println("Goodbye!")
          isStopped = true
      }
    }
  }

}
