package fyber.cli.common

import fyber.cli.common.Output.{Error, Empty}
import fyber.cli.common.Statement._
trait CliProcessor {
  def process(input: Array[String]): Output
}

trait AbstractProcessor extends CliProcessor {
  def isTaskDefined( id: Id[Task]): Boolean
  def isLinkDefined( source: Id[Task], target: Id[Task]): Boolean
  def onLink(source: Id[Task], target: Id[Task]): Unit
  def onTask(task: Task): Unit
  def onProcess(input: In): Output
  def onReset(): Unit

  def process(input: Array[String]): Output = {
    input(0) match {
      case ExitStm => Output.Exit
      case TaskStm =>
        val taskId = Id[Task](input(1))
        if(isTaskDefined(taskId)) Output.Error(s"Task '${taskId.value}' has already been defined")
        else {
            val operations = input.slice(2, input.length).map(operation)
            operations.find(_.isInstanceOf[Operation.UnknownOperation]) match {
              case None =>
                onTask(Task(taskId, operations))
                Empty
              case Some(operation: Operation.UnknownOperation) => Output.Error(s"Unknown operation: ${operation.name}")
              case Some(_) => Output.Error(s"Unknown operation")
            }
        }
      case LinkStm =>
        def unknownTaskError(id: Id[Task]) = Output.Error(s"Unknown task $id")

        val sourceTaskId = Id[Task](input(1))
        val targetTaskId = Id[Task](input(2))

        if ( !isTaskDefined(sourceTaskId) ) {
          unknownTaskError(sourceTaskId)
        } else if ( !isTaskDefined(targetTaskId) ) {
          unknownTaskError(targetTaskId)
        } else {
          onLink(sourceTaskId, targetTaskId)
          Empty
        }
      case ResetStm =>
        onReset()
        Empty
      case ProcessStm => onProcess(input.slice(1, input.length).mkString)
      case e: String => Output.Error(s"Unknown statement: $e")
    }
  }

  private def operation(name: String): Operation = name match {
    case Operation.DelayOp => new Operation.Delay()
    case Operation.ReverseOp => new Operation.Reverse()
    case Operation.NoopOp => new Operation.Noop()
    case Operation.EchoOp => new Operation.Echo()
    case _ => new Operation.UnknownOperation(name)
  }
}
