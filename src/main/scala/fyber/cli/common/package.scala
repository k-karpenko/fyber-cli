package fyber.cli

package object common {
  sealed trait Output {
    def asInput(): In
  }

  object Output {
    //    case object Exit extends Output
    case class Str(value: String) extends Output {
      override def asInput(): In = value
    }

    case class Error(value: String) extends Output {
      override def asInput() = ""
    }

    case object Empty extends Output {
      override def asInput(): In = ""
    }

    object Exit extends Output {
      override def asInput(): In = ""
    }

  }

  case class CliProcessException(msg: String) extends Exception

  case class Id[T](value: String)

  type In = String
  type Out = Output

  case class Run(input: In, prev: Option[Run] = None, output: Option[Output] = None)

  trait Operation {
    def execute(run: Run): Out
  }

  object Operation {
    val EchoOp = "echo"
    val NoopOp = "noop"
    val ReverseOp = "reverse"
    val DelayOp = "delay"

    class UnknownOperation(val name: String) extends Operation {
      override def execute(run: Run): Out = Output.Error(s"Unknown operation: $name")
    }

    class Echo extends Operation {
      override def execute(run: Run): Out = Output.Str(run.input + run.input)
    }

    class Noop extends Operation {
      override def execute(run: Run): Out = Output.Str(run.input)
    }

    class Reverse extends Operation {
      override def execute(run: Run): Out = Output.Str(run.input.reverse)
    }

    object Delay {
      val defaultValue = "tbb"
    }

    class Delay extends Operation {
      override def execute(run: Run): Out = Output.Str(run.prev.map(_.input).getOrElse(Delay.defaultValue))
    }
  }

  sealed trait Statement
  object Statement {
    val TaskStm = "task"
    val LinkStm = "link"
    val ProcessStm = "process"
    val ResetStm = "reset"
    val ExitStm = "exit"

    case class Task(name: Id[Task], operations: Array[Operation]) extends Statement

    case class Link(source: Id[Task], target: Id[Task]) extends Statement

    case class Process(input: Array[In]) extends Statement

  }
}
