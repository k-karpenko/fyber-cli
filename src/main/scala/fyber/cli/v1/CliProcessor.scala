package fyber.cli.v1

import fyber.cli.common._
object CliProcessor {

  case class ProcessorState( tasks: List[Statement.Task] = List(), links: Seq[Statement.Link] = Seq())

  sealed trait ExecutionStep
  object ExecutionStep {
    import Statement._
    case class UnitOfWork(task: Task) extends ExecutionStep {
      override def toString: String = s"Task(${task.name})"
    }

    case class SequentialWork(task: Seq[ExecutionStep]) extends ExecutionStep {
      override def toString: String = s"Seq(${task.mkString(", ")}"
    }

    case class MergeWork(in: SequentialWork, out: ExecutionStep) extends ExecutionStep {
      override def toString: String = s"Merge($out <- ${in.toString})"
    }
  }
}


class StandardCliProcessor extends AbstractProcessor {
  import CliProcessor.ExecutionStep._
  import Statement._
  import CliProcessor._

  var state = new ProcessorState()
  var executionPlan: Option[ExecutionStep] = None

  override def isTaskDefined(id: Id[Task]): Boolean = {
    state.tasks.exists(_.name == id)
  }

  override def isLinkDefined(source: Id[Task], target: Id[Task]): Boolean = {
    state.links.exists( l => l.source == source && l.target == target )
  }

  override def onLink(source: Id[Task], target: Id[Task]): Unit = {
    state = state.copy(links = state.links :+ Link(source, target))
    executionPlan = Some(buildExecutionPlan)
  }

  override def onProcess(input: In): Out = {
    val executionResult = execute( Run(input), executionPlan.get)
    executionResult.output.getOrElse(Output.Empty)
  }

  override def onReset(): Unit = {
    state = ProcessorState()
    Output.Str("[RESET] OK")
  }

  override def onTask(task: Task): Unit = {
    state = state.copy(tasks = state.tasks :+ task)
    executionPlan = Some(buildExecutionPlan)
  }

  def execute(run: Run, executionPlan: ExecutionStep): Run = executionPlan match {
    case UnitOfWork(task) =>
      val result = task.operations.foldLeft(run) { (l, r) =>
        l match {
          case Run(_, _, Some(Output.Error(_))) => l
          case Run(_, _, _) =>
            Console.out.println ("[OPERATION] task " + task.name.value + " with input " + l.input + " operation " + r.getClass.getName )
            val output = r.execute(l)
            val run = Run( output.asInput(), Some(l), Some(output))
            Console.out.println("[OPERATION] task " + task.name.value + " execution result: " + run.output.getOrElse(""))
            run
        }
      }
      result
    case SequentialWork(steps) =>
      steps.foldLeft(run) { (l, r) =>
        Console.out.println("[EXECUTION] Step " + r + ", input = " + l.input )
        execute(l, r)
      }

    case MergeWork(in, out) =>
      val merged = execute(run, in)
      execute(merged, out)
  }

  private def buildExecutionPlan: ExecutionStep =
    buildExecutionPlan(state.tasks.head)

  private def buildExecutionPlan(task: Task): ExecutionStep = {
    val links = state.links
      .filter(_.target == task.name)
      .flatMap(t => state.tasks.find(_.name == t.source))
    if (links.isEmpty) UnitOfWork(task)
    else MergeWork( SequentialWork(links.map(l => buildExecutionPlan(l))), UnitOfWork(task) )
  }

}
